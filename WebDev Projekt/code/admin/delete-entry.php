<?php
    if (isset($_GET['delete'])) {
        // required permissions: DELETE
        $dbHost = "127.0.0.1";
        $dbUser = "swisshotel";
        $dbPassword = "myswisshotel";
        $dbDatabase = "swisshotel";
        $dbTable = "reservation";

        // Create connection
        $mysqli = new mysqli($dbHost,$dbUser,$dbPassword,$dbDatabase);

        // Check connection
        if ($mysqli -> connect_errno) {
            echo "<h1>Failed to connect to MySQL</h1><p>: " . $mysqli -> connect_error . "</p>";
            exit();
        }

        // delete entry
        $mysqli -> query("DELETE FROM $dbTable WHERE ID=" . $_GET['delete'] . ";");

        $mysqli -> close();
    }
    // forward to index.html
    header('Location: index.php?success=true');
?>