// return true if everything is correct, otherwise return false
function testInput() {
    // define id's of all elements
    texts = ['first_name', 'last_name', 'location', 'adress'];
    numbers = ['number', 'plz', 'arrival', 'departure'];
    
    itemArrival = document.getElementById("arrival");
    itemDeparture = document.getElementById("departure");

    var dateArrival = new Date(itemArrival.value);
    var dateDeparture = new Date(itemDeparture.value);

    // set everything to invalid
    [].concat(texts, numbers, "arrival", "departure").forEach(element => { // gets EVERYTHING that needs to be checked
        document.getElementById(element).classList = "validate valid";
    });
    // set room selectors to valid (they are a bit weird because of the GUI-Framwork not being intended to be used like this)
    Array.from(document.getElementsByClassName("room-selector")).forEach(element => {
        element.children[0].children[0].classList = "select-dropdown dropdown-trigger valid";
    });


    // check if length of each element (get by id) is between 3 and 64
    texts.forEach(element => {
        item = document.getElementById(element);
        if (item.value.length < 3 || item.value.length > 64)
            item.classList = "invalid"; // mark as invalid
    });

    // check if length of each element (get by id) is more than 1, not negative and not longer than 12 digits
    numbers.forEach(element => {
        item = document.getElementById(element);
        if (item.value.length <= 1 || item.value.length > 12)
            item.classList = "invalid"; // mark as invalid
    });


    // check if date is ""
    if (itemArrival.value == "")
        itemArrival.classList = "invalid";

    // check if date is ""
    if (itemDeparture.value == "")
        itemDeparture.classList = "invalid";

    /*// checking if date isn't in past isn't needed because you cannot enter a date in the past or one that is before the arrival date anyways but it is here so you could enable it when needed
    // check if date is not in the past
    if (dateArrival.getTime() < dateNow.getTime())
        itemArrival.classList = "invalid";
    // check if date is not in the past
    if (dateDeparture.getTime() < dateNow.getTime())
    itemDeparture.classList = "invalid";*/

    // check if arrival date is not in the future of departure date (not necesairy but here for security reasons if someone is being a dum dum)
    if (dateArrival.getTime() >= dateDeparture.getTime())
        itemDeparture.classList = "invalid";

    // check that you have to reservate at least one room
    singleRoomElement = document.getElementById("countOfSingle");
    doubleRoomElement = document.getElementById("countOfDouble");
    if (singleRoomElement.value + doubleRoomElement.value < 1)
        // set room selectors to invalid (they are a bit weird because of the GUI-Framwork not being intended to be used like this)
        Array.from(document.getElementsByClassName("room-selector")).forEach(element => {
            element.children[0].children[0].classList = "select-dropdown dropdown-trigger invalid";
        });
    

    // if nothing is invalid, return true
    if (document.getElementsByClassName("invalid").length == 0)
        return true; 
    // otherwise return false
    M.toast({html: 'Ungültige Eingabe, bitte korrigieren!'});
    return false; 
}