-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 22. Apr 2020 um 18:06
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- 
-- Datenbank: `swisshotel`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `reservation`
--

CREATE TABLE `reservation` (
  `ID` int(11) NOT NULL,
  `Anrede` varchar(300) NOT NULL,
  `Vorname` varchar(300) NOT NULL,
  `Nachname` varchar(300) NOT NULL,
  `Adresse` varchar(300) NOT NULL,
  `PLZ` varchar(300) NOT NULL,
  `Ort` varchar(300) NOT NULL,
  `Land` varchar(300) NOT NULL,
  `Anreise` date NOT NULL,
  `Abreise` date NOT NULL,
  `AnzahlEinzelzimmer` int(11) NOT NULL,
  `AnzahlDoppelzimmer` int(11) NOT NULL,
  `Fruestueck` tinyint(1) NOT NULL,
  `Raucherzimmer` tinyint(1) NOT NULL,
  `Wellnessbereich` tinyint(1) NOT NULL,
  `Newsletter` tinyint(1) NOT NULL,
  `Reservationszeitpunkt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `reservation`
--

INSERT INTO `reservation` (`ID`, `Anrede`, `Vorname`, `Nachname`, `Adresse`, `PLZ`, `Ort`, `Land`, `Anreise`, `Abreise`, `AnzahlEinzelzimmer`, `AnzahlDoppelzimmer`, `Fruestueck`, `Raucherzimmer`, `Wellnessbereich`, `Newsletter`, `Reservationszeitpunkt`) VALUES
(1, 'Herr', 'Max', 'Muster', 'Mustergasse 1', '1234', 'Musterlingen', 'Schweiz', '2020-05-03', '2020-05-05', 2, 3, 1, 0, 1, 0, '2020-04-21 16:59:45'),
(2, 'Frau', 'Bettina', 'Master', 'Masterstrasse 2', '4321', 'Masterlingen', 'Schweiz', '2020-06-09', '2020-06-16', 0, 1, 0, 1, 0, 1, '2020-04-21 17:05:14');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `reservation`
--
ALTER TABLE `reservation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
