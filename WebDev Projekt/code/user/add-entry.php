<?php
    // required permissions: INSERT
    $dbHost = "127.0.0.1";
    $dbUser = "swisshotel";
    $dbPassword = "myswisshotel";
    $dbDatabase = "swisshotel";
    $dbTable = "reservation";

    $mysqli = new mysqli($dbHost,$dbUser,$dbPassword,$dbDatabase);

    // Check connection
    if ($mysqli -> connect_errno)
        echo "<h1>Failed to connect to MySQL</h1><p>: " . $mysqli -> connect_error . "</p>";

    // read in values
    $salutation = $_POST["salutation"];
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $location = $_POST["location"];
    $country = $_POST["country"];

    $plz = $_POST["PLZ"];

    $countOfSingle = $_POST["countOfSingle"];
    $countOfDouble = $_POST["countOfDouble"];

    // read in special variables
    $address = $_POST["adress"] . " " . $_POST["number"];
    $breakfast = isset($_POST['breakfast'][0]) ? 1 : 0;
    $smoking = isset($_POST['smoking'][0]) ? 1 : 0;
    $wellness = isset($_POST['wellness'][0]) ? 1 : 0;
    $newsletter = isset($_POST['newsletter'][0]) ? 1 : 0;

    // convert date
    $arrivalDate = date_create($_POST['arrival'])->format('Y-m-d H:i:s');
    $departureDate = date_create($_POST['departure'])->format('Y-m-d H:i:s');

    $error = false;
    // check if names are too long or too short
    foreach (array($salutation, $first_name, $last_name, $location, $country) as $value)
        if (strlen($value) < 3 || strlen($value) > 64) {
            echo $value. "Ein Fehler ist aufgetreten. Ein String ist zu kurz oder zu lang<br>";
            $error = true;
        }

    // check if plz is set
    if (strlen($plz) <= 1) {
        echo "Ein Fehler ist aufgetreten. Die Postleitzahl ist zu kurz<br>";
        $error = true;
    }
    // check if date is in correct order
    if (strtotime($_POST['arrival']) > strtotime($_POST['departure'])) {
        echo "Ein Fehler ist aufgetreten. Abreisedatum vor Ankunftsdatum<br>";
        $error = true;
    }
    //check if both dates are no in the past
    if (strtotime($_POST['arrival']) < strtotime('now') || (strtotime($_POST['departure']) < strtotime('now'))) {
        echo "Ein Fehler ist aufgetreten. Kann nicht in der Vergangenheit reservieren<br>";
        $error = true;
    }
    // check if at least one room has been reserved
    if ($countOfSingle + $countOfDouble < 1) {
        echo "Ein Fehler ist aufgetreten. Mindestens ein Raum muss reserviert werden<br>";
        $error = true;
    }
    

    if ($error == false) {
        // set query
        $sql = "INSERT INTO $dbTable (Anrede, Vorname, Nachname, Adresse, PLZ, Ort, Land, Anreise, Abreise, AnzahlEinzelzimmer, AnzahlDoppelzimmer, Fruestueck, Raucherzimmer, Wellnessbereich, Newsletter) VALUES ('$salutation', '$first_name', '$last_name', '$address', '$plz', '$location', '$country', '$arrivalDate', '$departureDate', '$countOfSingle', '$countOfDouble', '$breakfast', '$smoking', '$wellness', '$newsletter');";
    
        // execute query
        if ($mysqli -> query($sql)) {
            echo "New record created successfully";
        
            // forward to index.html
            header('Location: index.html?success=true');
        } else { echo "Error: " . $sql . "<br>" . $mysqli->error; }

        $mysqli -> close();
    }
?>