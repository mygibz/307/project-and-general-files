<?php
    // required permissions: SELECT
    $dbHost = "127.0.0.1";
    $dbUser = "swisshotel";
    $dbPassword = "myswisshotel";
    $dbDatabase = "swisshotel";
    $dbTable = "reservation";
?>

<!DOCTYPE html>
<html>
    <head>
        <!-- Import Materialize Style -->
        <link rel="stylesheet" href="css/materialize.min.css">
        <!-- Import Material Icons -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- Import and Initialize MaterialCSS scripts -->
        <script defer src="js/materialize.min.js"></script>

        <script src="js/script.js"></script>

        <title>Swiss Hotel - Buchung</title>
        <link rel="shortcut icon" href="src/logo.png" type="image/png">
        <script defer src="js/toast.js"></script>
        <link href="css/style.css" rel="stylesheet">

        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    </head>

    <body>
        <nav>
            <div class="nav-wrapper">
                <a href="#" class="brand-logo"><img src="src/logo.png" id="logo" alt="Swiss Hotel"></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="index.php" style="font-size: 2.1rem;">SwissHotel - Datenbank</a></li>
                </ul>
            </div>
        </nav>

        <div style="height: 10px"></div>

        <div class="container">
            <div class="row">
                <table>
                    <tr>
                        <th>ID</th>
                        <th>Anrede</th>
                        <th>Vorname</th>
                        <th>Nachname</th>
                        <th>Adresse</th>
                        <th>PLZ</th>
                        <th>Ort</th>
                        <th>Land</th>
                        <th>Anreise</th>
                        <th>Abreise</th>
                        <th>Zimmer</th>
                        <th>Zusätze</th>
                        <th>Reservationszeitpunkt</th>
                    </tr>
                    <?php
                        $mysqli = new mysqli($dbHost,$dbUser,$dbPassword,$dbDatabase);

                        // Check connection
                        if ($mysqli -> connect_errno) {
                            echo "<h1>Failed to connect to MySQL</h1><p>: " . $mysqli -> connect_error . "</p>";
                            exit();
                        }

                        if ($result = $mysqli -> query("SELECT * FROM " . $dbTable)) {
                            /* fetch associative array */
                            while ($row = $result -> fetch_assoc()) {
                                $breakfast = ($row['Fruestueck'] == "1") ? "<i class='material-icons tooltipped' data-position='top' data-tooltip='Breakfast included'>free_breakfast</i>" : "" ;
                                $smoking = ($row['Raucherzimmer'] == "1") ? "<i class='material-icons tooltipped' data-position='top' data-tooltip='Raucherzimmer'>smoking_rooms</i>" : "" ;
                                $wellness = ($row['Wellnessbereich'] == "1") ? "<i class='material-icons tooltipped' data-position='top' data-tooltip='Wellness'>spa</i>" : "" ;
                                $newsletter = ($row['Newsletter'] == "1") ? "<i class='material-icons tooltipped' data-position='top' data-tooltip='Newsletter'>mail</i>" : "" ;

                                $SingleRooms = ($row['AnzahlEinzelzimmer'] > 0) ? $row['AnzahlEinzelzimmer'] . "x <i class='material-icons tooltipped' data-position='top' data-tooltip='Einzeltimmer'>person</i> " : "" ;
                                $DoubleRooms = ($row['AnzahlDoppelzimmer'] > 0) ? $row['AnzahlDoppelzimmer'] . "x <i class='material-icons tooltipped' data-position='top' data-tooltip='Doppelzimmer'>people</i>" : "" ;

                                echo "<tr><td>" . $row['ID'] . "</td><td>" . $row['Anrede'] . "</td><td>". $row['Vorname'] . "</td><td> ". $row['Nachname'] . "</td><td>" . $row['Adresse'] . "</td><td>" . $row['PLZ']. "</td><td>" . $row['Ort']. "</td><td>" . $row['Land']. "</td><td>" . $row['Anreise']. "</td><td>" . $row['Abreise']. "</td><td>" . $SingleRooms .$DoubleRooms . "</td><td>" . $breakfast . $smoking . $wellness . $newsletter . "</td><td>" . $row['Reservationszeitpunkt'] . "<i class='material-icons tooltipped delete' data-position='top' data-tooltip='Delete this entry' onclick=\"deleteEntry(" . $row['ID'] .");\">delete_forever</i>" . "</td><td>" . "</td>";
                            }
                            $result->free();
                        }
                        $mysqli -> close();
                    ?>
                </table>
            </div>
        </div>

        <!-- Import and Initialize MaterialCSS scripts -->
        <script src="js/initialize-material.js"></script>
    </body>
</html>