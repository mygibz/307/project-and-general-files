document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.datepicker');
    let options = {
        firstDay: 1,
        minDate: new Date(),
        onClose() {
            let optionsDeparture = {
                firstDay: 1,
                minDate: new Date(document.getElementById('arrival').value)
            }
            M.Datepicker.init(document.querySelectorAll('#departure'), optionsDeparture);

            let optionsArrival = {
                firstDay: 1,
                minDate: new Date(),
                maxDate: new Date(document.getElementById('departure').value),
             };
             M.Datepicker.init(document.querySelector("#arrival"), optionsArrival);
        }
    }
    M.Datepicker.init(elems, options);
});

  
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems);
});
