function deleteEntry(id) {
    // delete entry if correct password
    var enteredPassword = prompt("Please enter your Password:", "");
    var client = new XMLHttpRequest();
    client.onload = function() {
        if (client.responseText == "1")
            window.location.href = 'delete-entry.php?delete=' + id;
        else
            M.toast({html: 'Falsches passwort eingegeben!'});
    }
    client.open("GET", "check-password.php?key=" + enteredPassword);
    client.send();
}