const urlParams = new URLSearchParams(window.location.search);
if (urlParams.get('success') == 'true') {
    // show success toast
    M.toast({html: 'Buchung erfolgreich erstellt!'});
    // rewrite url (without reloading) in oder to not display toast every time
    window.history.pushState("index.html", "SwissHotel", window.location.href.split("?")[0]);
}
